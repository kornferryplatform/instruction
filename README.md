# Instruction #

This README would normally document whatever steps are necessary to get your application up and running.

### PLEASE NOTE: npm version should be v4.0.5, node version should be v7.2.1 ###

### Instructions step by step ###
* clone three repos (kfhub_app, kfhub_lib, kfhub_tarc_lib) under same folder
* go to kfhub_lib
* run "(sudo) npm install"
* go to kfhub_tarc_lib
* run "(sudo) npm run rebuild:lib"
* run "(sudo) npm run start:dev"
* after the server starts, go to localhost:4200

if after you run start:dev and see errors like:

* return this.all[key];
*     	       ^

* npm ERR! Exit status 1


run "(sudo) npm run start:dev" again until the server starts.
